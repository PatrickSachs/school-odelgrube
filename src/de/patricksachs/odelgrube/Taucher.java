package de.patricksachs.odelgrube;

import java.util.Random;

/**
 * Created by patri on 03/02/2016.
 * Ein Schüler
 */
public class Taucher extends Thread {
    private final int MAX_HARNDRANG = 500; // Ab welchem Harndrang muss der Schüler auf's Klo?
    private final int FLASCHE_INHALT = 10; // Wie stark erhöht sich der Harndrang?
    private final int ODELSTRAHL_STAERKE = 70; // Wie stark verringert sich der Harndrang?
    private final int UNTERRICHT_LAENGE = 4000; // Wie lange muss der Schüler zwischen Puasen warten? (Millisekunden)
    private final int ODELSTRAHL_ERSCHOEPFUNG = 1500; // Wie lange muss der Schüler zwischen dem abgeben von Körperflüssigkeiten warten? (Millisekunden)

    //private Random random;
    private int harndrang;
    private Schulhaus schulhaus;
    private Odelgrube aktuelleGrube;
    private String name;

    public Taucher(Schulhaus schulhaus) {
        this.schulhaus = schulhaus;
        this.name = Utilities.getRandomName();
        this.harndrang = Utilities.getRandomLazy().nextInt(MAX_HARNDRANG);
        System.out.println("Neuer Schüler " + toString() + " mit " + (int)(((double)harndrang/(double)MAX_HARNDRANG) * 100) + "% Harndrang in der Schule angekommen!");
    }

    public void run() {
        while (true) {
            if (istAmOdeln()) {
                // Der Schüler ist bereits auf der Toilette; Harndrang verringert sich.
                harndrang -= Utilities.getRandomLazy().nextInt(ODELSTRAHL_STAERKE);
                if (harndrang <= 0) {
                    rausAusDerGrube();
                }
                try {
                    Thread.sleep(Utilities.getRandomLazy().nextInt(ODELSTRAHL_ERSCHOEPFUNG));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                // Der Schüler ist im Pausenraum; Harndrang erhöht sich.
                harndrang += Utilities.getRandomLazy().nextInt(FLASCHE_INHALT);
                if (harndrang >= MAX_HARNDRANG) {
                    aufDieGrube();
                }
                try {
                    // Der Schüler muss in den Unterricht und bekommt in dieser Zeit keinen neuen
                    // Harndrang.
                    Thread.sleep(Utilities.getRandomLazy().nextInt(UNTERRICHT_LAENGE));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //if (harndrang > 80000) {
            //    System.out.println("waiiit?");
            //}
            //System.out.println(toString() + "->" + harndrang);
        }
    }

    public synchronized String grubenName() {
        return aktuelleGrube != null ? aktuelleGrube.toString() : "";
    }

    public boolean istAmOdeln() {
        return aktuelleGrube != null;
    }

    public double getHarndrangPercent() {
        return ((double)harndrang / (double)MAX_HARNDRANG) * 100;
    }

    public boolean rausAusDerGrube() {
        if (istAmOdeln()) {
            if (aktuelleGrube.freigeben(this)) {
                aktuelleGrube = null;
                harndrang = 0;
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean aufDieGrube() {
        if (istAmOdeln()) {
            return false;
        }
        Odelgrube grube = schulhaus.findeFreienOdelplatz();
        // Konnte kein freier Odelplatz gefunden werden können wir auch nicht odeln.
        if (grube == null) {
            System.out.println(toString() + " möchte odeln, kann aber keinen freien Odelplatz finden! :(");
            return false;
        }
        if (grube.besetze(this)) {
            aktuelleGrube = grube;
            return true;
        }
        return false;
    }

    public String toString() {
        return name;
    }
}
