package de.patricksachs.odelgrube;

/**
 * Created by patri on 03/02/2016.
 * Diese Klasse stellt eine Toilette dar.
 */
public class Odelgrube {
    private int papierNochDa;
    private Taucher taucher;
    private String name;

    public Odelgrube() {
        name = Utilities.getRandomOdelName();
        papierNochDa = 10;
        System.out.println("Neue Odelgrube " + toString() + " eingerichtet!");
    }

    public synchronized int papierAnzahl() {
        return papierNochDa;
    }

    public synchronized void papierAuffuellen(int paper) {
        papierNochDa += paper;
        System.out.println("Neues Papier " + toString() + " verfügbar! Aktuelle gelagert: " + papierNochDa);
    }

    public synchronized boolean hatPapier() {
        return papierNochDa > 0;
    }

    public synchronized boolean verwendePapier() {
        if (hatPapier()) {
            papierNochDa--;
            return true;
        }
        return false;
    }

    public synchronized boolean besetze(Taucher neuerTaucher) {
        if (!kannGeodeltWerden()) {
            return false;
        }
        taucher = neuerTaucher;
        System.out.println("Odler " + neuerTaucher.toString() + " odelt jetzt " + toString() + ".");
        return true;
    }

    public synchronized boolean freigeben(Taucher aktuellerTaucher) {
        if (!odelt(aktuellerTaucher)) {
            return false;
        }
        if (!verwendePapier()) {
            return false;
        }
        taucher = null;
        System.out.println("Odler " + aktuellerTaucher.toString() + " hat fertig geodelt!");
        return true;
    }

    public synchronized String odlerName() {
        return taucher != null ? taucher.toString() : "";
    }

    public synchronized boolean kannGeodeltWerden() {
        return !wirdGeodelt() && hatPapier();
    }

    public synchronized boolean wirdGeodelt() {
        return !odelt(null);
    }

    public synchronized boolean odelt(Taucher einOdler) {
        return taucher == einOdler;
    }

    public String toString() {
        return name;
    }
}
