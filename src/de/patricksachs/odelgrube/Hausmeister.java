package de.patricksachs.odelgrube;

/**
 * Created by patri on 04/02/2016.
 * [CLASS DOCUMENTATION HERE]
 */
public class Hausmeister extends Thread {
    private final int PAPIER_PRODUKTION = 7;
    private final int BROTZEIT_LAENGE = 3000;
    private String name;
    private Schulhaus schulhaus;
    private int papierLager;
    private boolean intelligent;

    public Hausmeister(Schulhaus haus, boolean intelligent) {
        this.intelligent = intelligent;
        name = Utilities.getRandomName();
        papierLager = 0;
        schulhaus = haus;
    }

    public int gelagertesPapier() {
        return papierLager;
    }

    public void run() {
        while (true) {
            papierLager += Utilities.getRandomLazy().nextInt(PAPIER_PRODUKTION);
            Odelgrube grube = intelligent
                    ? schulhaus.findeOdelplatzMitWenigPappier()
                    : schulhaus.findeOdelplatzOhnePapier();
            if (grube != null) {
                gibPapier(grube);
            }
            try {
                Thread.sleep(Utilities.getRandomLazy().nextInt(BROTZEIT_LAENGE));
            } catch (InterruptedException e) {
            }
        }
    }

    public String toString() {
        return name;
    }

    public void gibPapier(Odelgrube grube) {
        grube.papierAuffuellen(papierLager);
        papierLager = 0;
    }

    public boolean isIntelligent() {
        return intelligent;
    }

    public void setIntelligent(boolean intelligent) {
        this.intelligent = intelligent;
    }
}
