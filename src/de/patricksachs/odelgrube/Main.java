package de.patricksachs.odelgrube;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Main {
    private static Schulhaus schulhaus;
    private static OdelGUI odelGUI;
    private static GUIManager guiManager;



    public static void main(String[] args) {
        schulhaus = new Schulhaus(1500, 100, true);
        odelGUI = new OdelGUI();
        guiManager = new GUIManager(schulhaus, odelGUI);
        guiManager.start();
    }

    public static Schulhaus getSchulhaus() {
        return schulhaus;
    }

    public static OdelGUI getOdelGUI() {
        return odelGUI;
    }

    public static GUIManager getGuiManager() {
        return guiManager;
    }
}
