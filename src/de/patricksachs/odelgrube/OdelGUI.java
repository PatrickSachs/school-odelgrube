package de.patricksachs.odelgrube;

import javax.swing.*;

/**
 * Created by patri on 03/02/2016.
 * [CLASS DOCUMENTATION HERE]
 */
public class OdelGUI extends JFrame {
    private JPanel rootPanel;
    private JList taucherListe;
    private JList odelListe;
    private JLabel klopapierDisplays;
    private JLabel hausmeisterDisplay;

    private DefaultListModel odelModel;
    private DefaultListModel taucherModel;

    public OdelGUI() {
        super("Odelgrube");
        setContentPane(rootPanel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setVisible(true);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    private void createUIComponents() {
        odelModel = new DefaultListModel();
        odelListe = new JList(odelModel);

        taucherModel = new DefaultListModel();
        taucherListe = new JList(taucherModel);

    }

    public void setDirty() {
        odelListe.setModel(odelModel);
        taucherListe.setModel(taucherModel);
    }

    public DefaultListModel getOdelListe() {
        return odelModel;
    }
    public DefaultListModel getTaucherListe() {
        return taucherModel;
    }

    public JLabel getKlopapierDisplays() {
        return klopapierDisplays;
    }

    public JLabel getHausmeisterDisplay() {
        return hausmeisterDisplay;
    }
}
