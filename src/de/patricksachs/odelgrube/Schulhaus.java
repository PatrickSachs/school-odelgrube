package de.patricksachs.odelgrube;

import java.util.ArrayList;

/**
 * Created by patri on 03/02/2016.
 * Diese Klasse dient als Toilettenverwalter.
 */
public class Schulhaus {
    private Odelgrube[] gruben;
    private Taucher[] taucher;
    private Hausmeister hausmeister;

    public Schulhaus(int schuelerZahl, int grubenZahl, boolean intelligenterHausmeister) {
        gruben = new Odelgrube[grubenZahl];
        taucher = new Taucher[schuelerZahl];
        for (int i = 0; i < grubenZahl; i++) {
            Odelgrube g = new Odelgrube();
            gruben[i] = g;
        }
        for (int i = 0; i < schuelerZahl; i++) {
            Taucher t = new Taucher(this);
            taucher[i] = (t);
        }
        hausmeister = new Hausmeister(this, intelligenterHausmeister);
        lassDieOdlerLos();
    }

    private void lassDieOdlerLos() {
        for (Taucher einTaucher : taucher) {
            einTaucher.start();
        }
        hausmeister.start();
    }

    public synchronized int grubenAnzahl() {
        return gruben.length;
    }

    public synchronized int taucherAnzahl() {
        return taucher.length;
    }

    public synchronized Odelgrube grubenNummer(int index) {
        return gruben[index];
    }

    public synchronized Taucher taucherNummer(int index) {
        return taucher[index];
    }

    public synchronized Odelgrube findeOdelplatzMitWenigPappier() {
        Odelgrube aktuell = gruben[0];
        for (int i = 1; i < gruben.length; i++) {
            Odelgrube neu = gruben[i];
            if (neu.papierAnzahl() < aktuell.papierAnzahl()) {
                aktuell = neu;
            }
        }
        return aktuell;
    }

    public synchronized Odelgrube findeFreienOdelplatz() {
        for (Odelgrube eineGrube : gruben) {
            if (eineGrube.kannGeodeltWerden()) {
                return eineGrube;
            }
        }
        return null;
    }

    public synchronized Odelgrube findeOdelplatzOhnePapier() {
        for(Odelgrube eineGrube : gruben) {
            if (!eineGrube.hatPapier()) {
                return eineGrube;
            }
        }
        return null;
    }

    public Hausmeister getHausmeister() {
        return hausmeister;
    }
}
