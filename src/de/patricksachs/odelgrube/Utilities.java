package de.patricksachs.odelgrube;

import java.util.Random;

/**
 * Created by patri on 03/02/2016.
 * Verschiedene Hilfsfunktionen
 */
public class Utilities {
    private static final String[] NAMEN = {"Herr", "Frau", "Sir", "Our Lord And Savior", "Lady", "Von Und Zu", "König", "Diktator", "Paul", "Michael", "Rosi", "John", "Bietmar", "Detlef", "Rüdiger", "Klaus", "Steve", "Reiner", "Werner", "Fabien", "Frank", "Adi", "Gertrude", "Chantalle", "Daniel", "Superman", "Batman", "Fuck Java", "Ingö", "Abdulah", "Bob", "Mama", "Papa", "Jürgen", "Till", "Daisuke", "Satomi", "Opa", "Oma", "Klaus-Frederik-Martin", "Timm", "Johann", "Maria", "Diana", "Donald", "Barack", "Polski", "Napoleon", "Putin", "Babocrafter2003", "Knugbag", "Kali", "Kolumbus", "Stalin", "Inf0rmatik0r"};
    private static final String[] NACHNAMEN = {"Merkel", "Cena", "Müller", "Maier", "Heider", "Mayer", "Schuhmacher", "Bauer", "Jobs", "Goldberg", "Siegismund", "Hain", "Meckel", "Lagerfeld", "van H. Inten", "Geschwendnor", "Fischer", "Weber", "Wolf", "Schneider", "König", "Roi Soleil", "Schnullilord", "Weithofer", "PvP", "on YouTube", "Peter-Schuhbert", "der Baumeister", "Vogel", "Kobayashi", "Fujiwara", ""};
    private static final String[] ODELNAMEN = {"in der Aula", "in der Biologiesammlung", "in der Bücherei", "in Schwendners Bienengrube", "in der neuen Reichskanzlei", "in Herrn Boshofs Büro", "auf Herrn Boshofs Schreibtisch", "in Herrn Boshofs Trinkhorn", "im Lehrerzimmer", "neben der Schultoilette", "im Meditationsraum", "im Sekreteriat", "in 104 Nord", "im Informatikraum", "im Pausenhof", "neben dem Süssigkeitenautomaten", "in der Asozialenecke", "im Spind", "im Kollegstufenzimmer", "vor dem Vertretungsplan", "in der Abstellkammer", "in der Sportumkleide", "in der Schwimmhalle", "im Büro des Systemadministrators", "auf dem Kopierer", "im Blumentopf", "in der Hose", "vor der Storchenkamera", "in der Realschule", "im Busbahnhof", "auf dem Schuldach"};
    private static Random random;

    public static Random getRandomLazy() {
        if (random == null)
            random = new Random();
        return random;
    }

    public static String getRandomName() {
        int vorIndex = getRandomLazy().nextInt(NAMEN.length);
        int nachIndex = getRandomLazy().nextInt(NACHNAMEN.length);
        return NAMEN[vorIndex] + " " + NACHNAMEN[nachIndex];
    }

    public static String getRandomOdelName() {
        int index = getRandomLazy().nextInt(ODELNAMEN.length);
        return ODELNAMEN[index];
    }
}
