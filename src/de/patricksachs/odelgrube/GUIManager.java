package de.patricksachs.odelgrube;

import javax.swing.*;

/**
 * Created by patri on 04/02/2016.
 * [CLASS DOCUMENTATION HERE]
 */
public class GUIManager extends Thread {
    private final int REFRESH_RATE = 100;
    private OdelGUI odelGui;
    private Schulhaus schulhaus;

    public GUIManager(Schulhaus haus, OdelGUI gui) {
        JOptionPane.showMessageDialog(null,
                "Willkommen in der Odelgrube!\nHier wird das Harndrangverhalten von Schülern eines deutschen Gymnasiums HOCHREALISTISCH simuliert.\n\nDisclaimer: Sämtliche Namen sind ZUFÄLLIG generiert und sollen nicht beabsichtigt auf reale Personen anspielen!", "Odelgrube - Hinweis!", JOptionPane.OK_OPTION);

        odelGui = gui;
        schulhaus = haus;

        setUpGui();
    }

    private void setUpGui() {
        DefaultListModel odelListe = odelGui.getOdelListe();
        int odelZahl = schulhaus.grubenAnzahl();
        for (int i = 0; i < odelZahl; i++) {
            odelListe.addElement("[" + i + "] " + grubenString(schulhaus.grubenNummer(i)));
        }

        DefaultListModel taucherListe = odelGui.getTaucherListe();
        int taucherZahl = schulhaus.taucherAnzahl();
        for (int i = 0; i < taucherZahl; i++) {
            taucherListe.addElement("[" + i + "] " + taucherString(schulhaus.taucherNummer(i)));
        }
        odelGui.getHausmeisterDisplay().setText("Hausmeister - " + schulhaus.getHausmeister().toString());
    }

    private void updateGui() {
        DefaultListModel odelListe = odelGui.getOdelListe();
        int odelZahl = schulhaus.grubenAnzahl();
        for (int i = 0; i < odelZahl; i++) {
            odelListe.setElementAt("[" + i + "] " + grubenString(schulhaus.grubenNummer(i)), i);
        }

        DefaultListModel taucherListe = odelGui.getTaucherListe();
        int taucherZahl = schulhaus.taucherAnzahl();
        for (int i = 0; i < taucherZahl; i++) {
            taucherListe.setElementAt("[" + i + "] " + taucherString(schulhaus.taucherNummer(i)), i);
        }
        odelGui.getKlopapierDisplays().setText(schulhaus.getHausmeister().gelagertesPapier() + "");
        //System.out.println("gui update");
        //odelGui.setDirty();
    }

    private String grubenString(Odelgrube grube) {
        return grube.toString() + " (Belegt? "
                + (grube.wirdGeodelt() ? "<JA>" : "nein")
                + (" | Papier: " + grube.papierAnzahl()) + ")"
                + (grube.wirdGeodelt() ? " ... " + grube.odlerName() : "");
    }

    private String taucherString(Taucher taucher) {
        return taucher.toString() + " (Odelt? "
                + (taucher.istAmOdeln() ? "<JA>" : "nein")
                + " | Harndrang: " + (int)taucher.getHarndrangPercent() + "%)"
                + (taucher.istAmOdeln() ? " ... " + taucher.grubenName() : "");
    }

    public void run() {
        while (true) {
            updateGui();
            try {
                Thread.sleep(REFRESH_RATE);
            } catch (InterruptedException e) {
            }
        }
    }
}
